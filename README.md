I am currently taking this course, and it's going to be discontinued soon. I don't want to lose the materials in it, so I am archiving it to the best of my ability.

At the time of this archiving, this is Saylor Academy's policy on sharing their materials:

> © Saylor Academy 2010-2024 except as otherwise noted. Excluding course final exams, content authored by Saylor Academy is available under a Creative Commons Attribution 3.0 Unported license. Third-party materials are the copyright of their respective owners and shared under various licenses. See detailed licensing information <https://www.saylor.org/open/licensing-information/>.
>
> Saylor Academy®, Saylor.org®, and Harnessing Technology to Make Education Free® are trade names of the Constitution Foundation, a 501(c)(3) organization through which our educational activities are conducted.
>
>One explicit exception is final exam content: the questions and answers that comprise our exam “banks” and/or an individual exam attempt are not licensed for reuse and must not be copied, adapted, posted or distributed without permission (students may keep a copy of their exam for personal, offline use as provided on the review page for each exam attempt).

I don't know if this applies to discontinued content, so I'll have to reach out to them about that if I want to be able to include the final exam in the archive. 