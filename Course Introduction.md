# Course Introduction

Time: 86 hours

How do we define revolution? In 1970, the Chinese leader Zhou Enlai said that the outcome of the French Revolution was unclear – 200 years is too soon to determine its long-term effects! Revolutions are complex and nuanced and can shift the global balance in fundamental ways – from England's Glorious Revolution in the 1600s to the United States, France, Haiti, Russia, China, and the modern independence movements in Southeast Asia, Africa, Latin America, and the Middle East. In this course, we explore the causes of revolution, analyze the ideologies that inspired the revolutionaries, examine the use of violence, and consider how historical revolutions have shaped contemporary politics. Our exploration includes reading and evaluating critical historical sources.
Most revolutionaries rose to protest a failure of Thomas Hobbes' Social Contract – these instigators felt their government no longer served the needs of its people or had become unresponsive and oppressive. The protesters use different methods – from relatively peaceful civil disobedience in India; to extreme violence in France, Russia, China, and Cambodia; to subversive terrorist tactics against colonialism in the United States, Latin America, Vietnam, and the modern Middle East.

Although the jury is still out, several revolutions created stable representative governments, such as in India, Japan, the United Kingdom, and the United States. Did China and Russia create stable governments, or did the autocratic leaders simply stifle discontent? As Zhou Enlai said, history will decide. Many countries elected or appointed a series of failed leaders who prompted counter-revolutionaries to rise in civil wars and subsequent revolutions. This occurred in France, Haiti, Latin America, and the Middle East. Many of the revolutions we study in this course were direct responses to colonialism and European imperialism, such as in the United States, Haiti, Latin America, Southeast Asia, Vietnam, India, and the Middle East.

Revolutions come in many forms: political, social, agricultural, and scientific. We begin by examining the nature of political revolution and how pre-revolutionary Europe and the Enlightenment have shaped modern revolutions. We will also explore European colonialism and imperialism since the oppression the foreign powers caused prompted revolutionaries to rebel in search of independence. By the end of this course, you will be able to discuss the nature of political revolution, identify commonalities and differences among these events, and understand how they individually and collectively transformed the modern world.

## Course Syllabus

See `Course Syllabus.md` for more information.

## Unit 1: The Glorious Nature of Revolution

How do we define political revolution? While debated, most historians define revolution as a transformation of a political system that is often accompanied by violence. However, the fundamental factors that cause revolutions are still debated. In this unit, we examine the nature of revolution, the Enlightenment in Europe, and how this led to the Glorious Revolution of the 17th century.

**Completing this unit should take you approximately 8 hours.**

## Unit 2: The American Revolution

By the mid-eighteenth century, Britain had colonies and trading posts across the globe. During this period of colonialism and European imperialism, European countries carved up the world – taking large portions of Africa, Asia, and the Americas. While some European powers used these newly-acquired territories to establish homes for their people, most viewed their colonies as an opportunity to harvest natural and human capital to enrich the "mother country". The colonies and the people who lived there were not equal partners. By the 18th century, England had colonies in India and the Americas.

While the American colonies produced certain cash crops such as tobacco, the British monarchy and government largely ignored them because they were not as rich in resources as their other colonies. While some colonies had royal governors, most were allowed to govern themselves and were not taxed. The American colonies were left alone, in a state of relative salutary neglect.

This changed in 1763 when the American colonies got caught up in the Seven Years' War (1756–1763), one of the many conflicts between France and England, the two major superpowers of the colonial era. England emerged victorious from this conflict which the American colonists called the French and Indian War. It effectively pushed France out of North America.

Despite gaining a great amount of territory, the war was costly to the British Empire. To recoup its losses, the British government famously imposed new taxes on its American colonies. In addition to this sudden change in British governance, the Proclamation Line of 1763 blocked the American colonists from accessing the territory gained from war. The British had agreed to cede this land to the Native Americans who had been their allies during the Seven Years' War.

**Completing this unit should take you approximately 8 hours.**

## Unit 3: The French Revolution and Its Legacy

Many argue that the French Revolution was the most important modern revolution. The revolutionary leaders abolished the monarchy and altered most of France's social and political institutions to make them more rational and modern. They proclaimed a republic, instituted parliamentary elections, introduced educational reforms, created a new revolutionary calendar, and reorganized France's electoral districts to make representation more democratic.

Unlike the American Revolution, which resulted in a government that has lasted until today, the French revolutionaries rejected their initial ideals when the new government began to use violence and terror to maintain its hold on power. By 1799, the revolution succumbed to a dictatorship at the hands of Napoleon Bonaparte (1769–1821). As the self-proclaimed emperor, Napoleon expanded his empire, plunging Europe into 15 years of conflict, shifting alliances, and French domination. The Napoleonic Wars (1799–1815) reshaped political boundaries in Europe and indirectly resulted in revolutions around the world.

The French Revolution (1789–1799) abolished the monarchy and transformed France's political system into a republic, a government where elected officials hold power. While elections and many public policy reforms were put into place, the revolution was extremely drastic and violent. It also resulted in the ascension of Napoleon and the reorganization of Europe. In this unit, we explore the roots and impact of the French Revolution, the ascension and importance of Napoleon, the partitioning of Poland and Lithuania, and the Congress of Vienna of 1815 that reorganized Europe. As you study, think about how it may have been inspired by the American Revolution and how it helped shape the modern world.

**Completing this unit should take you approximately 11 hours.**

## Unit 4: Revolution in Haiti, Mexico, Latin America, and the Philippines

The American and French Revolutions inspired revolutions in Mexico, Latin America, and the Caribbean. Mexico experienced several revolutions starting in the 19th century and extending into the 20th century. Spain controlled Mexico, Latin America, and the islands of Puerto Rico and Cuba. Haiti, in contrast, was controlled by France. Spain and France had colonies worldwide from the Americas to Africa to Asia and the Pacific. As we have learned, the ambitions of Napoleon disrupted Europe.

In 1808, Napoleon had deposed the Spanish King Ferdinand VII (1784–1833) and created political instability in Spain. These distractions caused Spain to lose its colonial control over Mexico, Latin America, and its islands in the Caribbean and Pacific Oceans. The French Revolution also directly contributed to the Haitian Revolution and the liberation of Haiti. In this unit, we explore the revolutions in Haiti, Mexico, Latin America, and the Philippines. As you read, think about how the American and French Revolutions inspired revolution throughout the colonial world.

**Completing this unit should take you approximately 10 hours.**

## Unit 5: Nations, Empires, Imperialism, and Peoples in an Industrializing Age

So far, we have focused on political revolution. While political upheaval and the reorganization of political boundaries have greatly impacted the development of society as we know it, social, scientific, and industrial revolutions have also shaped our modern world. Before the so-called "modern age" featured mass European exploration and colonization, Europe had existed on the sidelines of global history. Innovation, intellectualism, and trade were centered in the Middle East and Asia. After the Roman Empire fell in the 4th century, Europe plunged into a 1,000-year period known as the Dark Ages. Most Europeans forgot about the advances the Romans had made in philosophy, science, and technology, while innovation continued elsewhere around the globe.

In the Middle East, the Islamic Caliphates created great libraries and universities to maintain the knowledge of the ancient Greeks and Romans. They also studied the advances in philosophy, science, and engineering in ancient China and Korea. Around 1000, the Sung Dynasty invented gunpowder and produced the first gun. Choe Yun-ui, a Korean inventor, created the first movable type metal printing press in Korea in 1234, 200 years before Gutenburg's printing press. During the 11th century, China's Sung Dynasty began using paper currency, and the Mongolian Yuan Dynasty created a system of banking in the 14th century. The Islamic Caliphates created the first hospitals, physician training, and medical texts, while Abu Ali Sina (980–1037) (also called Avicenna) penned the Canon of Medicine. Many Europeans consider Avicenna the father of early modern medicine and pharmacology.

Since Europe had lived in intellectual darkness for so long, their reawakening in the scientific, philosophical, and medical fields probably seemed even more remarkable and exciting during Europe's scientific (1500–1600) and industrial revolutions (1800's). Europe began to reassert itself on the world stage in the 1400s. Their explorers and traders pushed into Africa, Asia, and the Americas, where they absorbed but frequently annihilated the knowledge, culture, and populations of the places they conquered.

European imperialism also revolutionized slavery. Although slavery had existed since the dawn of human civilization, European slavery centered on capturing and forcing all levels of African societies into enslavement without an ability to escape or earn their freedom from their new lives of forced labor. Europeans popularized the idea of skin-based racism, a new social construct that continues to stratify and divide communities to this day.

In this unit, we examine how European imperialism, slavery, and the scientific and industrial revolutions impacted culture, social class, and the rights of people. As you read, investigate, and explore, remember how the Enlightenment philosophers inspired the political revolutions we have studied so far. Consider how these political revolutions resulted from European imperialism and the other social events and ideas we will discuss in Unit 5.

**Completing this unit should take you approximately 12 hours.**

##Unit 6: The Russian Empire and Revolutions of 1905 and 1917 and Their Legacy

The Russian revolution had a profound impact on the world stage. It involved a complete transformation from a monarchical to a communist system and led to socialist movements in Cuba, North Korea, China, Africa, and Southeast Asia.

In this unit, we investigate the Russian Empire, the cultures and religions that shaped Russian society, the revolutions of 1905 and 1917, and the formation of the Soviet Union. As you progress through the unit, think about how the Industrial Revolution and the ideals of the Enlightenment influenced communism. World War I also influenced the Russian Revolution of 1917.

**Completing this unit should take you approximately 5 hours.**

## Unit 7: The Effects of Colonialism on Asia

European imperialism has touched every corner of the globe. In Asia, colonialism devastated India and destabilized China's Qing Dynasty. American imperialism galvanized Japan and sparked rapid industrialization that led to Japan's own form of imperialism against Korea and China during World War II. The French colonized Indochina (today's Vietnam, Laos, and Cambodia). Siam (today's Thailand) was able to withstand European imperialism by serving as a cosmopolitan go-between for English-controlled India and French-controlled southeast Asia. European and American imperialism ended centuries-long dynasties in India, China, and Japan and led to revolutions that changed the political foundations of these countries.

In 1854, Great Britain dismantled India's 300-year old Mughal Dynasty, its last ruling dynasty, and reorganized India into a colonial entity to exploit its natural resources. In 1930, Mahatma Gandhi (1869–1948), an Indian lawyer, anti-colonial nationalist, and political ethicist who employed nonviolent resistance, led India's rebellion against Britain's colonial rule to gain independence in 1947.

Indochina served as a colony for France in Southeast Asia beginning in 1887. Ho Chi Minh's Vietnamese rebel forces fought alongside the Allied powers to resist Japan's Imperial Army during World War II. But Vietnam and the rest of Indochina returned to French imperial rule at the end of the war. Ho Chi Minh (1890–1969) led a 30-year war for independence that split Vietnam into two countries and reunited it as a communist nation in 1975 when it expelled the United States from its borders.

Europe never fully colonized China (although Hong Kong was a British colony from 1841 to 1997). China experienced two revolutions during the 20th century that reshaped its social and political institutions. In 1911, nationalist forces overthrew the Qing Dynasty to establish a republican government. But their democratic experiment did not last long. The country soon fell into anarchy, and the Chinese Communist Party began a long fight with the nationalists for political control starting in 1921.

After an eight-year armistice to respond to the Japanese invasion (the second Sino-Japanese war from 1937–1945), Mao Zedong (1893–1976) rose to eventually defeat the nationalist forces to form a communist government in China in 1949. The People's Republic of China imposed radical large-scale land reform and dramatic industrial development, which contributed to China's extraordinary economic growth but caused widespread suffering and the death of millions of Chinese citizens.

Like China, Japan resisted European colonial efforts. Japan experienced an unexpected civil war due, in part, to coercion from American and European forces. Fearing a similar fate to what had occurred in mainland China, Japan industrialized rapidly, upended a 300-year government, and began colonizing mainland Asia. These actions of Japanese aggression included genocide, enslavement, and forcing Chinese women into prostitution.

In this unit, we investigate the impact of European and American imperialism in Asia and how it led to a series of revolutions across the continent and island nations that altered Asia politically, economically, and socially. We also evaluate the international consequences of these revolutions for global history.

**Completing this unit should take you approximately 17 hours.**

## Unit 8: The Collapse of Empires in the Middle East and Asia

In this unit, we explore the consequences of World War I and World War II. European imperialism revolutionized geopolitics and defined the development of the modern world. It also promoted feelings of nationalism, a sentiment that galvanized countries, led to revolutions, and shaped their interactions with the world. The growth of nationalism directly contributed to the outbreak of World War I, when the Serbian nationalist Gavrilo Princip assassinated Archduke Franz Ferdinand of Austria on June 28, 1914, to protest Austrian control of Serbia. As we learned in Unit 3, Europe's web of alliances drew the European continent (and the entire world due to their colonial holdings) into the war.

World War I fundamentally disrupted the European world order and ultimately led to the rise of fascism in the form of Adolph Hitler (1889–1945) in Germany and Benito Mussolini (1883–1945) in Italy. Just as Napoleon had before him, Hilter took advantage of the political and economic instability to push German nationalists on the need to conquer Europe. Meanwhile, Japan moved into mainland Asia, and England and France carved up the remains of the Ottoman Empire in the Middle East. The Zionist movement began to gain momentum, but the atrocities holocaust convinced the world of the need to create a separate Jewish homeland in 1945.

World War II cemented the new world order. But the victors had learned about the need to reclaim geopolitical stability despite the physical destruction. The United States helped Europe and Japan rebuild. But the Middle East remained dangerously divided, and the United States and Soviet Union, the world's two new superpowers, would embark on a Cold War that would maintain relative peace, using their stockpiles of nuclear weapons as leverage.

**Completing this unit should take you approximately 14 hours.**

## Study Guide

This study guide will help you get ready for the final exam. It discusses the key topics in each unit, walks through the learning outcomes, and lists important vocabulary terms. It is not meant to replace the course materials!

See `Study Guide.md` for more information.


## Certificate Final Exam (coming soon)
Take this exam if you want to earn a free Course Completion Certificate.

To receive a free Course Completion Certificate, you will need to earn a grade of 70% or higher on this final exam. Your grade for the exam will be calculated as soon as you complete it. If you do not pass the exam on your first try, you can take it again as many times as you want, with a 7-day waiting period between each attempt.

Once you pass this final exam, you will be awarded a free Course Completion Certificate.
