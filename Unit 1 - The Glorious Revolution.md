# Unit 1: The Glorious Nature of Revolution

How do we define political revolution? While debated, most historians define revolution as a transformation of a political system that is often accompanied by violence. However, the fundamental factors that cause revolutions are still debated. In this unit, we examine the nature of revolution, the Enlightenment in Europe, and how this led to the Glorious Revolution of the 17th century.

**Completing this unit should take you approximately 8 hours.**

Upon successful completion of this unit, you will be able to:

- describe the nature and effects of revolution;
- explain how the Enlightenment affected the Glorious Revolution;
- explain how the Glorious Revolution influenced future revolutions in Eurasia and the Americas;
- explain how the Enlightenment philosophers contributed to our understanding of revolution; and
- compare revolts, coup d'etats, mass uprisings, and revolutions.

## 1.1: What is Revolution?

While revolutions come in many forms, they generally involve dramatic political change accompanied by varying degrees of violence: as a new government or thought process replaces the established order. In Latin, the word revolution literally means **to turn around**. This course explores revolution from a multidisciplinary perspective (sociology, political science, and history). We will study their causes and the ideologies that spark revolution.

From a political science perspective, revolution refers to a drastic and profound change of political power. Most major historic revolutions are multidimensional and bring about new political systems, economic paradigms, and revised cultural and social norms. In sociology, revolution refers to a fundamental and drastic change in the societal structure.

The key to understanding revolutions is that they have an effect beyond the initial mass uprising. A **mass uprising** or **protest** occurs when a local population protests or resists their government, in a relatively spontaneous manner, often in violence. Participants are less concerned about planning changes to the societal structure than other types of political rebellion. Examples of mass uprisings include the German Peasant Rebellion from 1524–1525 and the Sioux Ghost Dance of 1890. While the masses revolted, they did not fundamentally change the socio-political structure.

- [ ] 📖 Mass Protests and the Military (See `Mass Protests and the Military.md`)

> Read this article to learn about mass uprisings and how governments react to them. As you read, think about the nature of mass uprisings and how they can lead to revolution. How do governments and militaries respond to mass uprisings and protests? Does their reaction stifle protest, or does it inspire an uprising to evolve into a rebellion or revolution?

- [ ] 📺 The Long 19th Century (See `The Long 19th Century.md`)

> While the peasant or lower classes usually spearhead popular mass uprisings from below, small groups of the social elite typically initiate coup d'etats. These are highly organized and may or may not have popular support. For example, chosen military staff may suddenly replace political leaders with a violent revolt during a military coup, such as the coup d'état in Egypt in 1952 where Mohammed Naguib (1901–1984) and Gamal Abdel Nasser (1918–1970) overthrew Egypt's King Farouk (1920–1965). Similarly, the military deposed and incarcerated the democratically-elected Aung San Suu Kyi during a military coup d'etat in Myanmar in 2021 despite mass protests.
>
> Rebellions combine aspects of mass uprisings and coups. They are often the first step toward a revolution. They involve large numbers of participants and a high degree of planning and organization. The leaders typically have a clear vision for the future and enlist action from a large percentage of the population. The American Revolution began as a rebellion. So did the Russian Revolution in 1917 and the Chinese Communist Revolution in 1956.
>
> Watch this video to learn how revolutions shape the world, compared to a mass uprising, coup d'etat, or rebellion. The presenter explores the definition of revolution and examines how conditions in the 17th century fostered an era of modern revolution. How do political revolutions differ from agricultural and industrial revolutions?

- [ ] 📖 The Two Effects of Revolution (See `The Two Effects of Revolution.md`)

> Revolutionary change may be intellectual or political. Political change involves government overthrow, such as when colonists in the United States (1765–1783) and citizens of France (1789–1799) rebelled against monarchical power to support democracy. Intellectual change occurs when individuals challenge beliefs about how the world functions, such as when scientists and philosophers questioned the natural order and religious traditions during the Scientific Revolution after Nicolaus Copernicus published De Revolutionibus Orbium Coelestium (On the Revolutions of the Heavenly Spheres) in 1543.
>
> Do revolutions need to be violent to effect change? Many dubbed Czechoslovakia's revolution in 1989 the "velvet revolution" because the mass protests were essentially nonviolent. Leaders made compromises to avoid mass violence, which led some to argue it was not a true revolution. If violence is an essential component of a revolution, how do we define a peaceful transfer of power?
>
> Read this article on the nature and consequences of revolution. What are some root causes of revolution? How do these factors instigate social and political change?
>
> What is a revolution, and how does it compare to other kinds of uprisings? Why is it important to differentiate a mass uprising, revolt, or coup d'etat from a revolution?

## 1.2: Ideologies of Revolution

Revolution was a topic of great debate from the 17th to 20th centuries when prominent scholars helped define what revolution means, why it occurs, and its impact. Let's explore the ideologies of some key theorists and philosophers.

**Alex de Tocqueville** (1805–1859), a French political historian, believed that revolution was an inevitable outgrowth of **tyranny**. It will result when monarchies and other state agencies centralize all power and decision-making in their own hands. The elite, nobility, middle, and merchant classes will eventually rise up in protest to oppose these centralizing efforts that interfere with their liberties and privileges, such as excessive taxation and interference in their ability to own and control their private property. The elite prompts revolution to protect their own interests.

**Karl Marx** (1818–1883), a German philosopher and revolutionary activist, is known for his views on social or class conflict within society. He witnessed the extreme poverty that capitalist practices caused during the industrial revolution. He wrote about a rising struggle between the lower and upper classes over control of the means of production. Marx advocated that the working class (the Proletariat) should rise up in revolt against the upper classes (the Bourgeoisie) to support better working conditions. Marx predicted society would become more stable and equal once capitalism fails. Communism is the solution Marx predicts in which property becomes publicly owned. According to his political philosophy, each individual will work as much as they are able and will be paid according to their need. Marx's ideologies of revolution, in contrast to Tocqueville, arise from the bottom in which the Proletariat rises against the Bourgeoisie to replace the class system itself, in addition to the government.

**Crane Brinton** (1898–1968), an American historian, argued that revolution is simply part of the natural order of human development – a "fever" rages through the body politic and causes certain symptoms when intellectuals become alienated and stop believing in the political system. The intellectuals may merge with other groups, such as the middle, working, or peasant classes, to remove the old order through revolution. These coalitions often fall apart, with complaints about how the revolution was "betrayed", meaning it failed to follow the agenda of the first protesters. Thermidor refers to the period of reaction that follows revolution, when moderates resume or regain control, to overcome the radicals and restore stability. Brinton believed that growing societies may have to endure a revolutionary fever until a "normal" and "healthy" state of social equilibrium is restored until the next round of protests erupts and the revolutionary cycle occurs again.

**Chalmers Johnson** (1931–2010), the American author, argued that revolution results when the social equilibrium, social order, or sense of balance that stable communities exhibit is disrupted. Disequilibrium results from social change or dysfunction, such as industrialization, mass urbanization, or the rise of new social classes. Revolution results when the people at the top – an intransigent elite – refuses to adapt, adjust, or reform in response to the new environment. Samuel Huntington (1927–2008) presents a variant of this theory by saying revolution results when societies fail to cope with modernization and mass mobilization.

**Benedict Anderson** (1936–2015), an American sociologist, argued that governments often provoke revolution when they inflame nationalist sentiments to create a new nationalist identity. Modern ways of thinking undermine the old universal belief structures, such as the traditional ties to religion and multi-ethnic empires.

- [ ] 📖 Conflict Theory and Society (see `Conflict Theory and Society.md`)

> Read this lesson on the ideas of Marx. It explores his ideology which helps lay a foundation for what revolutions are and how they form. Think about the definition of revolution and how Marx's ideals contrast with those of Tocqueville, Johnson, and other philosophers.

- [ ] 🗒️ What Is the Tocqueville Effect? (See `What Is the Tocqueville Effect.md`)

> Read this article that explores Alex de Tocqueville's ideas and his basic theory on why revolution occurs. How did Tocqueville's ideas compare with those of Marx, Brinton, Johnson, and Anderson?

## 1.3: Paving the Way to Revolution

In this course, we focus on modern revolutions. Most historians think about modernity as a historic, social, economic, and political phenomenon that occurred after the Middle Ages and is associated with progress, reason, and science. Historians and sociologists (1950-70) embraced this concept of modernity which encompassed the processes of industrialization and the creation of nation-states.

This shift to modernity was characterized by a change in socioeconomic class structures and revolution.

Francis Bacon (1561–1626) saw the rise of a bourgeois class in Europe as merchants began to accumulate capital and concentrate wealth. Cracks in the edifice of feudalism emerged, and power re-oriented around a life that centered in court around the monarch. The European printing press allowed individuals and intellectuals to organize their thinking and share information with each other.

Capitalism, industrialization, and secularization took hold in the 18th century, creating fundamental changes in class structure that would lead to uprising and revolution. Throughout this period, exploration and colonialism allowed financial capital to accumulate in Western Europe that helped fuel the Industrial Revolution.

Global InequalityBook
Read this article on how the concept of modernity was centered in Western Europe. How did industrialization and the socioeconomic stratification it created lead to revolution? What linkages can you make between the concept of modernity and the ideologies we studied in the previous section: Tocqueville, Marx, Brinton, Johnson, and Anderson?

Modern RevolutionPage
Max Weber (1864–1920) argued that before the rise of the modern state, the king had to share his legitimate use of violence or force with the church. A defining feature of the modern state is that it alone has the ability to exert legitimate, coercive force. European governments no longer had to cooperate with the church to exert authority because the Catholic Church had lost much of its hegemonic power during the Enlightenment and Scientific Revolution. This shift, however, would also empower the people to question the legitimacy of governments – they determined that citizens had an innate right to rise in revolt and revolution.

Dutch and British ExceptionalismPage
John Merriman argues that the turmoil of the Thirty Years War (1618–1648) led many members of the nobility and upper classes to agree to the terms of monarchy and absolute rule in exchange for a restoration of public order, protection against popular insurrection, and peasant uprisings, and the recognition of noble privilege.

For example, the ancien régime in France describes this era where religious and other conventions supported a social and political order that glorified the king as protector and subjugated the peasants and lower classes. These monarchies routinely ignored any parliaments or government assemblies.

However, in England and the Netherlands, the parliament refused to be ignored. These countries each had a growing influential commercial middle-class population, a large number of property owners, a strong urban population, a small nobility, and a decentralized police force and army. Both resisted the power of the Catholic church based in Rome in favor of more local control.

Historical and intellectual elements of the Enlightenment contributed to revolutionary processes across the world. These changes and revolutions all contributed to the rise of modernity (which we explore below).

Watch this lecture, which discusses how and why both England and Holland rejected absolutist rule.

Absolutism and the StatePage
As you watch this lecture, try to understand how the Thirty Years War (1618–1648) influenced state-building in France and England. Focus on the following aspects of pre-revolutionary societies: the institution of kingship, the role of religion, and the mechanisms of taxation.

The Enlightenment and the Public SpherePage
During the Enlightenment, several key philosophers shaped the way people viewed the Enlightenment itself and the growing socio-economic and political changes it engendered.

Immanuel Kant (1724–1804), a German philosopher, encouraged individuals to think for themselves rather than exhibit blind obedience to political authority. Kant wrote that enlightenment is built on man's ability to use his own reason, which takes courage. He argued that most people reject enlightenment out of cowardice and laziness: they are unwilling to break away from the domination of others, particularly church leaders, government officials, and educators. Domination by these powerful people restricts one's individual freedom. Kant believed he did not live in an enlightened age, but in an age that was moving toward enlightenment, and that people would gradually learn to think for themselves over time.

Jean-Jacques Rousseau (1712–1778), a Swiss philosopher, wrote "The Social Contract", in which he argued that humans are born free but coerced into economic and social dependence. Political and social covenants should restore this lost freedom. Rousseau believed that legitimate civil authority is only derived from civil contracts that both sides enter freely. In other words, to be legitimate, citizens must enter a civil contract with a government willingly. In this civil society, each individual works for their own best interest. Collectively, these individual wills support and benefit the general will, which Rousseau called "the sovereign". In this system, the will of the majority rules. Each person who enters this social contract agrees to abide by laws the government passes, even when they disagree with them.

Nicolas de Condorcet (1743–1794), a French philosopher and mathematician, believed continual progress led toward the perfection of mankind. He argued that the progress the Enlightenment promoted, especially in the areas of science and social thought, would lead to an increasingly perfect human state. Understanding health, wealth creation, and industry would eventually lead to the elimination of disease, poverty, and suffering. Through knowledge, humans are capable of unlimited progress.

Watch this lecture. Focus on the different meanings of the Enlightenment among the intellectual elites and in popular culture. What did the Enlightenment thinkers focus on? What did they critique? How was the influence of the intellectuals different from that on the "street"?

What Is Enlightenment? by Immanuel KantPage
This 1784 essay is one of the most important texts of the European Enlightenment. What do you think Kant means by the "emergence from self-incurred immaturity"? What is the relationship between thinking for oneself and obedience to political authority? What do you think is revolutionary about this text?

The Social ContractBook
In The Social Contract, Rousseau articulated the concept of the general will, an idea the revolutionaries in France and other countries we will study frequently evoked. Read this selection of excerpts from Rousseau's influential text to understand what he meant by the concept of the general will.

The Future Progress of the Human MindPage
Read this 1794 essay and analyze its tone. Why do you think Condorcet is so unshakably convinced about the necessity of progress? What does he mean by progress? What is his understanding of history?

The EnlightenmentBook
The Enlightenment or Age of Reason (1715–1789) describes the period when philosophers and intellectuals emerged outside traditional religious spheres to question the established social and political order. Primary philosophical concepts included skepticism of the political establishment, the pursuit of reason, religious tolerance, liberty, and empiricism.

John Merriman says the enlightenment thinkers affected their readers in six ways:

They weakened the hold of traditional religion as a public institution;
They promoted a secular code of ethics;
They developed a critical spirit of analysis that did not accept routine tradition and hierarchies;
They were curious about history and believed in progress;
They differentiated absolutism from despotism; and
They disrespected monarchy and heaped abuse on unjustified privilege.
Read this article on the Enlightenment. Think about Merriman's six tenants of the Enlightenment and how they helped pave the way toward the great political revolutions of the modern era.

1.4: The Glorious Revolution
The English Civil War, which predated the Enlightenment, is also called the English Revolution. It was a period of armed political and social conflict that occurred from 1642 to 1660 and resulted in the creation of a constitutional monarchy. In early 1640, intellectuals met to discuss the ideas of liberty and individual rights.

The "levellers" and "diggers" championed principles of political freedom and equality. They published and distributed pamphlets to spread their ideas that inspired future intellectuals in France and the American colonies to revolt against their own governments.

The English Civil War saw a radical shift in social relationships that came to support a free-market capitalist system that did away with feudalism. In 1648, the army purged Parliament of its conservative members, Charles I (1600–1649) was executed in January 1649 due to his resistance to change, and Oliver Cromwell (1599–1658), the leader of the army, was installed to rule over the Parliament as the leader of the new commonwealth from 1653 to 1658. Based on our definition, were Cromwell's actions a revolution or a coup d'etat?

Oliver CromwellBook
Read this article to learn about Cromwell, his actions, and his importance. Think about Cromwell's actions and whether you believe he was a true revolutionary.

The Political Development of the British StateBook
Although the monarchy was restored in 1660 with Charles II, this "revolution" saw many accomplishments:

The rise of parliamentary supremacy over the Crown;
The idea that careers are open to talent;
Protection of private property (which supported the rise of mercantilism and capitalism);
Religious tolerance; and
Aggressive internationalism.
Many call the period between 1688 and 1689 the Glorious Revolution, before the Parliament passed the Bill of Rights in 1689 and signaled the official beginning of a constitutional monarchy.

Read this text and list the long-term causes of the English Revolution. Pay particular attention to the conflicts between religion and politics in 17th century England.

How Glorious was the Glorious Revolution?Page
Both revolution and civil war refer to dramatic and violent uprisings that express popular discontent. Both terms refer to upheaval within a particular country instead of international war. However, revolutions are generally uprisings against the current government. Civil war pits two or more opposing organized groups against each other, typically aligned within a country along ethnic, political, or religious lines. They typically engage in violent conflict with the goal of obtaining political power or control.

The historical determination on whether a conflict constitutes a civil war or revolution is not always clear-cut. For example, historians continue to disagree about the case of the English Civil War. Many call the conflict the English Civil War because two distinct groups battled against each other. However, others categorize the conflict as a revolution because the opponents ultimately fought against the government itself (the monarchy) and transformed the system of government into a constitutional monarchy.

Watch this lecture to understand the lasting impact of the Glorious Revolution. Think about how the Glorious Revolution changed how people viewed government, their role in it, and their right to interfere and overthrow it.

1.5: Government, Citizens' Rights, and Religion versus the State
The English Parliament introduced the Bill of Rights (1689) to denounce King James II (1633–1701), the last Roman Catholic monarch of Great Britain, for abusing his political power. Political and religious tensions were high: Protestants and Catholics were in conflict, as did Parliament and the monarchy.

The primary goals of the English Bill of Rights were to:

- Condemn the abuse of power by King James II;
- Detail civil freedoms in 13 articles;
- Clarify the rules of succession to the throne; and
- Reinforce the principles of the two founding documents of England's constitutional monarchy: the Magna Carta (1215) and the Petition of Right (1628).

The Magna Carta (1215) was the first charter to support the "rule of law" and civil liberties in Europe: it declared the King was not above the law and could not deprive anyone of their "land, castles, liberties, or rights" without "the lawful judgment of his peers". The Petition of Right (1628) reinforced the legal principles of the Magna Carta and added that the King could not impose his will on Parliament, tax without parliamentary approval, or support a standing army.

800 Years of the Magna CartaPage
Watch this video to understand the impact and importance of the Magna Carta. How did this transformative document justify the actions of Cromwell and other leaders during and after the Glorious Revolution?

The English Bill of Rights of 1689Page
With the passage of the English Bill of Rights (1689), Parliament supported the rule of law and the civil rights outlined in the two founding documents. It declared that it alone (not the Crown) had the authority to levy taxes, raise an army, and wage wars. The King and others in positions of authority were responsible and answerable to the people. The Bill also required regular parliamentary meetings, free elections, freedom of speech in Parliament, prohibitions against cruel and unusual punishment, and declared that judges would be independent of the monarchy.

As you read this document, focus on the kinds of rights it guarantees citizens. Test your understanding by writing a paragraph about how this document helps explain the difference between a traditional and constitutional monarchy.

Hobbes on Authority, Human Rights, and Social OrderBook
When King James II fled England in 1648, his Protestant daughter Mary II (1662–1694) assumed the English throne with her Dutch husband, William III (1650–1702), also known as William of Orange. Both supported Parliament's efforts to expand its power.

To protest the restrictive and prohibitive nature of King James' Catholicism and outside control from the Pope, Parliament declared no Roman Catholic could ascend the English throne, nor could any English monarch marry a Roman Catholic. The Toleration Act of 1689 supported freedom of worship for Protestants but excluded Catholics, antitrinitarians, and atheists from its provisions.

Thomas Hobbes (1588–1679), an English philosopher, laid the foundation for modern concepts of government and revolution. Hobbes was a royalist, meaning he believed that government power was best vested in a monarch. However, he proposed the idea of a social contract in which citizens and government form a cooperative relationship. People give up some of their rights; in return, the government provides them protection. However, what happens when the government fails to provide basic protections for the people? Do the people have the right to rebel?

Read this article which links the revolutionary experience in England and John Locke's Two Treatises on Government. Focus on the second half of the article (after Leviathan: Structure and Major Themes) and make sure you can discuss the logic of Locke's thinking about the powers and role of government in modern societies. Try to summarize Locke's ideology in a few sentences.

A Letter Concerning TolerationPage
John Locke (1632–1704), the English philosopher, also feared that Catholicism would take over England and argued for religious freedom in his Letter Concerning Toleration (1689). He argued the government existed to promote external, not spiritual, welfare and should not attempt to dictate religious choice. Locke believed that civil unrest and conflict would ensue when civil magistrates tried to limit the religious choices of citizens. In other words, Locke paved the way for the American idea of the separation of church and state. However, it is important to note that Locke was not opposed to suppressing religions that refused to accept his doctrine of tolerance.

Read "A Letter Concerning Toleration" by John Locke and "Liberty and Prosperity: The Levellers and Locke" by Murray Rothbard to understand what Hobbes and Locke thought about the relationship between politics, religion, the people, and the monarch. These discussions provide a foundation for the intellectual discussions revolutionaries will have during conflicts yet to come: the rights of the people, their relationship with the government, and their right to revolt or revolution. Where does Locke draw the boundaries of tolerance? How does he justify this choice? How does Hobbes see the relationship between the Crown and the People? How do Hobbes and Locke compare?"

A Short History of Human RightsPage
With the English Bill of Rights (1689), the English Parliament established its supremacy over the Crown and proclaimed the government a constitutional monarchy. Contrast this to an absolute monarchy, where the king or queen governs with absolute power and authority. In a constitutional monarchy, the country's written and unwritten constitution limits the powers of the monarch (the king or queen).

In today's United Kingdom (England, Scotland, Wales, and Northern Ireland), the monarch has limited formal authority and represents the country as its head of state, performing a primarily-public ceremonial role. The elected prime minister is the head of government: the parliament is responsible for debating, creating, and executing laws and overseeing and approving government taxation and spending.

Britain does not have a single written constitution like most modern states. Instead, its unwritten or uncodified constitution is based on Acts of Parliament, court judgments, and convention. Many of these traditional protocols originated with the Magna Carta (1215), the Petition of Right (1628), and the English Bill of Rights (1689). Note that English "common law" describes law derived from custom and judicial precedent rather than legislative statutes (statutory law). 

However, the importance of the evolution of English Common Law is in its assertion of human rights. The idea that the government exists for the betterment of the people and not the other way around – as was the common belief in feudalism – became the foundation of modern revolutions.

Watch this video which explores how human rights are integral to modern revolutions. Think about the importance of The Magna Carta (1215), the Petition of Right (1628), and the English Bill of Rights (1689). How does the assertion of rights lead to empowerment and revolution?

The Magna Carta versus the Bill of RightsBook
The Glorious Revolution, the English Bill of Rights (1689), and the Enlightenment profoundly influenced the revolutions that followed. The leaders of the American Revolution, in particular, cited the influence of philosophers, such as Hobbes and Locke, as they asserted their right to overthrow a government they felt had become corrupt. While the Glorious Revolution did not result in a total change in government, its lasting effect cannot be debated. With the passage of the English Bill of Rights (1689), the rights of the citizenry became the foundation for many modern revolutions that reshaped geopolitics in the 18th, 19th, and 20th centuries.

Read this essay that analyzes the various traditions that influenced the writing of the U.S. Constitution. The English Bill of Rights was among its most important influences. Then examine the chart that reviews the Magna Carta. How does this analysis compare with your reading of the English Bill of Rights? How did this document pave the way for the U.S. Constitution?

Formative Documents on Citizens' RightsPage
Read this article as part of your comparative exercise. How did each document pave the way toward the American Revolution and U.S. Constitution?
The Levellers and LockePage
Read this article about how the concepts of liberty and property were central to Locke's treatise and how religious liberty was central to the ideas of Locke and the ideas of the Enlightenment. How does religious liberty contribute to a population's feelings of empowerment toward revolution?

Unit 1 Assessment
Unit 1 AssessmentQuiz
To do: Receive a grade
Take this assessment to see how well you understood this unit.

This assessment does not count towards your grade. It is just for practice!
You will see the correct answers when you submit your answers. Use this to help you study for the final exam!
You can take this assessment as many times as you want, whenever you want.