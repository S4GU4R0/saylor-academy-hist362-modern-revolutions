Note: this is a summary, and it's worth reading the entire original article linked at the bottom of the first section

# Mass Protests and the Military

Read this article to learn about mass uprisings and how governments react to them. As you read, think about the nature of mass uprisings and how they can lead to revolution. How do governments and militaries respond to mass uprisings and protests? Does their reaction stifle protest, or does it inspire an uprising to evolve into a rebellion or revolution?

---

## Introduction

In nonviolent mass protests against dictators, the military is the ultimate arbiter of regime survival. Drawing on a global survey of forty "dictator's endgames" from 1946 to 2014, this essay examines how dictators and their militaries respond to popular protests, and what the consequences are in terms of the survival of authoritarianism or the emergence of democracy. The authors argue that the type of the authoritarian regime and the military's legacy of human rights violations go a long way in explaining whether a military will employ violence against the protesters or defect from the ruling coalition.

Over the last four decades, countries ruled by authoritarian regimes have seen a proliferation of nonviolent mass protests. Whether called "people power" movements, color revolutions, campaigns of nonviolent resistance, or nonviolent revolutions, these organized demands for democratic change have challenged dictators across wide swaths of the world. Such movements have succeeded to varying degrees and in different ways, but scholars agree that the response of state-security forces - and especially the military - is key. The lesson might be summed up this way: If a protest movement wants to trigger a transition toward democracy, one of the best things it can do is to spur a loyalty shift within the military.

The emphasis on the military is important. Some studies conflate military behavior during authoritarian regime crises with actions undertaken by state-security forces in general. That is a mistake. Security forces comprise an array of distinct institutions - not just the regular, uniformed military but also intelligence agencies, various kinds of police services, government militias, and maybe even hired thugs and goons. For a dictator, however, deploying troops trained and equipped to wield military-grade force is the last line of defense. If soldiers and tanks appear in the streets, it is a sure sign that the dictatorship's threat assessment has started to "flash red": The hour when routine repression could have put a lid on things has passed, and the regime faces a state of emergency. The stakes mount, as does the potential for serious violence. Protesters must prepare to brave graver physical dangers, regime insiders find it harder to coordinate among themselves, and top military officers become able to demand political and economic concessions in exchange for defending the incumbent.

It is also important to note that protesters are likely to help their cause by remaining nonviolent: Two recent quantitative studies suggest that the nonviolent character of mass uprisings in and of itself positively influences the likelihood of elite defections and campaign success.

Few studies have investigated the relationship between military reactions to nonviolent protest campaigns and subsequent political outcomes. The relationship is not dispositive, of course: A dictator can fall even if the military attacks nonviolent protesters. Conversely, military support for such protests cannot guarantee that successful democratization will come about. Many causal factors, some contingent and some structural, are at play in determining whether an autocratic ruler or regime stands or falls, and what happens afterward. One set of authoritarian incumbents might be replaced by a new one, or protests might be met by a successful crackdown that sets the stage for renewed authoritarianism.

Although peaceful protests are common under authoritarianism, by themselves they pose a fairly minor threat to the survival of autocrats and authoritarian regimes. According to one dataset, only 7 percent of all autocratic leaders who fell between 1950 and 2012 did so due to nonviolent mass protests. By contrast, fully a third of all these fallen authoritarians succumbed to military or other insiders' coups. Yet the incidence of coups, especially the full-scale military kind, has sharply declined since the 1980s, while the number of leader exits through nonviolent mass protests in autocracies has substantially risen.

The paradoxical character of these trends - leader exits becoming more common even as coups become rarer - may result from the complexity of the real-world cases on which these statistics are based. Popular protests, the threat of a coup d'état, and a revolution by the citizens may have close interconnections, so that what appears to be a "regular" leader exit, for instance, may actually be the result of a soft palace coup. For instance, Erich Honecker stepped down as the head of East Germany's ruling communist party and Slobodan Milošević resigned as president of Serbia "voluntarily". Yet in each case, mass protests were a crucial part of the story. Honecker gave poor health as his reason for leaving in October 1989, but in truth other members of the East German communist elite had been pressing him to quit out of worry that his hard-line approach to escalating mass protests would take them all down (they got rid of the unpopular Honecker but soon fell anyway). In 2000, similarly, Miloševiæ opted not to mention the ongoing protests in his resignation statement, preferring to cite instead a decision by the national election commission, but everyone knew that he was only saving face.

Miloševiæ's fall was a preview of Georgia's 2003 Rose Revolution and Ukraine's 2004 Orange Revolution in that mass protests erupting after a suspect election were the key event. In early 2011, long-ruling dictator Hosni Mubarak found himself relieved of the presidency of Egypt by a bloodless military coup although there had been no triggering election. Yet the mass protests of the Arab Spring had been roiling the country, and these moved the Egyptian army to act against him. In another vein, the protests in Libya and Syria that same year began as exercises in nonviolence, but were met with brutal military assaults by the Muammar al-Qadhafi and Bashar al-Assad dictatorships, respectively. In each country, the situation soon degenerated into a bloody, multisided civil war, with the regular military splitting up to fight on different sides.

---

Source: Aurel Croissant, David Kuehn, and Tanja Eschenauer, <https://muse.jhu.edu/article/698924>
Creative Commons License This work is licensed under a Creative Commons Attribution 4.0 License.

---

## The Dictator's Endgame

The problems of differentiating among overlapping empirical phenomena are too many to solve in a single essay. Our more modest aim, therefore, is to better understand how military leaders respond to one particular type of popular protest, and how the response they choose affects regime trajectories from that point.

Our focus is on situations in which a mostly peaceful mass protest movement demands regime change, or at least the dictator's resignation. Prior attempts, if any, to demobilize the protests through negotiations and political liberalization have failed. While the dictator tries to cling to power, police and other domestic security forces prove unable to contain swelling demonstrations. The dictator then feels a need to deploy the military. The threat or reality of military force becomes the necessary (though not sufficient) means to ensure the dictator's political survival. We call this situation the "dictator's endgame".

How the military responds helps to determine the prospects for leader exit or survival, regime collapse or renewal, and the emergence or failure of democracy. During a campaign of mass protest, military leaders must decide whether to defend the dictator against the opposition, which requires the application of some degree of military-grade violence against unarmed opponents, or to defect from the dictator. Most scholars assume it to be the default situation that a nation's military can behave as a coherent, unitary actor. They also tend to assume that a military establishment - like any large and complex bureaucratic organization - has institutional interests of its own that its members seek to advance, as well as certain broad political outcomes that it fears or favors. A final assumption is that the leaders at the top of the military hierarchy have the most influence in defining and acting on behalf of such vested "military interests".

A military's defection from a regime can take a softer or a harder form. The softer form is a loyalty shift; the harder form is the making of a coup. A loyalty shift takes place when the military declines to apply large-scale military force despite the dictator's order or request that it do so. Some militaries have defected by simply remaining in their barracks, as the South Korean army did in 1960 and the military forces of communist Czechoslovakia and East Germany did in 1989.

A more active form of loyalty shift occurs when a military, without actually launching a coup, nonetheless signals its support for the regime's opponents. This happened when the armed forces backed anti-Marcos "people power" in the Philippines in 1986. It occurred in Romania three years after that - at least at first, for within days soldiers tried and shot dictator Nicolae Ceauşescu and his wife. And it played out in Tunisia in early 2011, when smiling troops climbed out of their armored vehicles to fraternize and take pictures with demonstrators who had turned out to denounce the regime of President Zine al-Abidine Ben Ali. Staying quartered and showing support for the opposition both signal that the dictator can no longer count on the military.

The second and harder type of defection is the coup d'état, when the military (sometimes working with civilian elites) tries to unseat the dictator and seize power, if only temporarily. Examples include the Egyptian events of 2011 already mentioned, as well as events in Pakistan in March 1969, when General Yahya Khan declared martial law and installed a military government, and Haiti in early 1986, when General Henri Namphy took power at the head of a mixed military-civilian interim government after dictator Jean-Claude "Baby Doc" Duvalier had fled to France.

The alternative to soft or hard defection is "repression," meaning the organized use of large-scale military violence by the armed forces against protesters with the aim of putting down the mass unrest. Nonviolent mass protests that were met with brutal force include workers' protests in Poland in 1956 and 1970; student protests in China in 1989; the "8-8-88 Uprising" and the 2007 "Saffron Revolution" in Burma; and the campaigns of unarmed protest in Bahrain, Libya, and Syria in early 2011.

The decision to defect is something that takes place on the institutional level. It should not be confused with politically or morally motivated desertions by individual soldiers or officers, which may take place after troops have engaged the protesters. In Syria, President Assad successfully ordered crack military units to move violently against protesters; later, this triggered a significant number of troop desertions. In China in June 1989, there were isolated reports of soldiers fraternizing with demonstrators, but no intact unit joined the opposition when the People's Liberation Army moved in with Type 59 main battle tanks and soldiers killed hundreds of unarmed protesters in and around Beijing's Tiananmen Square.

---

## Trends and Patterns

Based on our strict criteria for classification, we examined the hundreds of major civil-resistance campaigns listed in the Nonviolent and Violent Campaigns and Outcomes (NAVCO) v.2.0 dataset to identify cases that satisfied our definition of a "dictator's endgame". Because NAVCO v.2.0 covers only the years 1946 through 2006, and because the variables included in the dataset do not differentiate between the military and other security forces, we did additional research. This consisted of surveying other databases, studying a wealth of scholarly and media sources, and interviewing experts – all with the goals of extending coverage through 2014 and categorizing the military's initial reaction in each case going all the way back to 1946. In order to determine whether protests took place under a given autocratic regime, we used and updated the Autocratic Regimes Dataset.

The forty cases listed in the Table represent 51 percent of all 79 mass-protest campaigns included in our augmented version of NAVCO v.2.0. Each of these 79 campaigns was a) predominantly nonviolent, b) directed against the incumbent regime, and c) aimed at regime change or at least a change of leader. To form the Table, we eliminated any case that lacked at least medium-scale repression, in keeping with the assumption that the dictator will seek to call in the military only after the civilian security apparatus has tried and failed to stop the protests. (See our online Appendix for a more detailed description of how we identified the endgames included in the Table). As a group, the regimes cases shown in the Table represent all the major types of authoritarian governments. They exhibit varying levels of repressiveness and come from an array of geographical regions; historical, cultural, and political circumstances; and decades, starting with the 1950s and ending with the 2010s. A country may appear in Table twice (as do Egypt, Poland, and Thailand) or even thrice (as does South Korea), owing to distinct episodes that took place within it at different times.

As the data indicate, the most likely military reaction to a nonviolent mass-protest movement is repression, with a loyalty shift being second-likeliest and a coup being the least likely response. Thus in nearly half the cases (19), the military engaged in large-scale repression. In another 15 cases (or 37 percent), the military shifted its loyalty from the dictator to the opposition. Only in six cases did the military stage a coup. The great bulk (32) of these "dictator's endgame" scenarios took place after 1980, with close to a third of the total (12) coming since the year 2000.

These results cohere with the findings of recent research regarding nonviolent revolutions. Most of our "endgames" fall into one of three temporal clusters. These clusters correlate, respectively, with: 1) the democratic revolutions of the "third wave" of global democratization during the 1980s and 1990s; 2) the "color revolutions" of the 2000s; and 3) the Arab uprisings of 2011. The largest cluster, comprising 21 cases, is the one that correlates with the third wave. In five of the seven cases before the third wave (these early cases span the years from 1956 through 1973), militaries opted to repress protests. Likewise, in the eight

<table border="1" width="80%" cellpadding="10" cellspacing="”10" style="text-align: center;">
    <tbody>
        <tr>
            <td width="“18%”">&nbsp;<p></p></td>
            <td width="14%"><strong>Year</strong></td>
            <td width="14%"><strong>Military Response</strong><p></p></td>
            <td width="14%"><strong>Leader Exit within 1 Year</strong></td>
            <td width="14%"><strong>Transition within 3 Years</strong></td>
            <td width="14%"><strong>Democracy after 5 Years</strong><p></p></td>
        </tr>
        <tr>
            <td colspan="6"><strong><em>Before 1974</em></strong></td>
        </tr>
        <tr>
            <td width="“18%”">Poland</td>
            <td style=" text-align: center;" td="" width="“16%”">1956</td>
            <td style="text-align: center;" td="" width="“16%”">R</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
        </tr>
        <tr>
            <td width="“18%”">Venezuela</td>
            <td style="text-align: center;" td="" width="“16%”">1958</td>
            <td style="text-align: center;" td="" width="“16%”">R</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
        </tr>
        <tr>
            <td width="“18%”">South Korea</td>
            <td style="text-align: center;" td="" width="“16%”">1960</td>
            <td style="text-align: center;" td="" width="“16%”">L</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
        </tr>
        <tr>
            <td width="“18%”">Pakistan</td>
            <td style="text-align: center;" td="" width="“16%”">1969</td>
            <td style="text-align: center;" td="" width="“16%”">C</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
        </tr>
        <tr>
            <td width="“18%”">Poland</td>
            <td style="text-align: center;" td="" width="“16%”">1970</td>
            <td style="text-align: center;" td="" width="“16%”">R</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
        </tr>
        <tr>
            <td width="“18%”">Greece</td>
            <td style="text-align: center;" td="" width="“16%”">1973</td>
            <td style="text-align: center;" td="" width="“16%”">R</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
        </tr>
        <tr>
            <td width="“18%”">Thailand</td>
            <td style="text-align: center;" td="" width="“16%”">1973</td>
            <td style="text-align: center;" td="" width="“16%”">R</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
        </tr>
        <tr>
            <td colspan="6"><strong><em>1974–99 (incl. Third Wave)</em></strong></td>
        </tr>
        <tr>
            <td width="“18%”">Iran</td>
            <td style="text-align: center;" td="" width="“18%”">1978</td>
            <td style="text-align: center;" td="" width="“16%”">R</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
        </tr>
        <tr>
            <td width="“18%”">South Korea</td>
            <td style="text-align: center;" td="" width="“18%”">1980</td>
            <td style="text-align: center;" td="" width="“16%”">R</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
        </tr>
        <tr>
            <td width="“18%”">Argentina</td>
            <td style="text-align: center;" td="" width="“18%”">1982</td>
            <td style="text-align: center;" td="" width="“16%”">L</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
        </tr>
        <tr>
            <td width="“18%”">Pakistan</td>
            <td style="text-align: center;" td="" width="“18%”">1983</td>
            <td style="text-align: center;" td="" width="“16%”">R</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
        </tr>
        <tr>
            <td width="“18%”">Sudan</td>
            <td style="text-align: center;" td="" width="“18%”">1985</td>
            <td style="text-align: center;" td="" width="“16%”">C</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
        </tr>
        <tr>
            <td width="“18%”">Haiti</td>
            <td style="text-align: center;" td="" width="“18%”">1986</td>
            <td style="text-align: center;" td="" width="“16%”">C</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
        </tr>
        <tr>
            <td width="“18%”">Philippines</td>
            <td style="text-align: center;" td="" width="“18%”">1986</td>
            <td style="text-align: center;" td="" width="“16%”">L</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
        </tr>
        <tr>
            <td width="“18%”">South Korea</td>
            <td style="text-align: center;" td="" width="“18%”">1987</td>
            <td style="text-align: center;" td="" width="“16%”">L</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
        </tr>
        <tr>
            <td width="“18%”">Burma</td>
            <td style="text-align: center;" td="" width="“18%”">1988</td>
            <td style="text-align: center;" td="" width="“16%”">R</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
        </tr>
        <tr>
            <td width="“18%”">China</td>
            <td style="text-align: center;" td="" width="“18%”">1989</td>
            <td style="text-align: center;" td="" width="“16%”">R</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
        </tr>
        <tr>
            <td width="“18%”">CSSR</td>
            <td style="text-align: center;" td="" width="“18%”">1989</td>
            <td style="text-align: center;" td="" width="“16%”">L</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
        </tr>
        <tr>
            <td width="“18%”">GDR</td>
            <td style="text-align: center;" td="" width="“18%”">1989</td>
            <td style="text-align: center;" td="" width="“16%”">L</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
        </tr>
        <tr>
            <td width="“18%”">Romania</td>
            <td style="text-align: center;" td="" width="“18%”">1989</td>
            <td style="text-align: center;" td="" width="“16%”">L</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
        </tr>
        <tr>
            <td width="“18%”">Albania</td>
            <td style="text-align: center;" td="" width="“18%”">1990</td>
            <td style="text-align: center;" td="" width="“16%”">L</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
        </tr>
        <tr>
            <td width="“18%”">Bangladesh</td>
            <td style="text-align: center;" td="" width="“18%”">1990</td>
            <td style="text-align: center;" td="" width="“16%”">L</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
        </tr>
        <tr>
            <td width="“18%”">Madagascar</td>
            <td style="text-align: center;" td="" width="“18%”">1991</td>
            <td style="text-align: center;" td="" width="“16%”">L</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
        </tr>
        <tr>
            <td width="“18%”">Mali</td>
            <td style="text-align: center;" td="" width="“18%”">1991</td>
            <td style="text-align: center;" td="" width="“16%”">R</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
        </tr>
        <tr>
            <td width="“18%”">Thailand</td>
            <td style="text-align: center;" td="" width="“18%”">1992</td>
            <td style="text-align: center;" td="" width="“16%”">R</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
        </tr>
        <tr>
            <td width="“18%”">Malawi</td>
            <td style="text-align: center;" td="" width="“18%”">1993</td>
            <td style="text-align: center;" td="" width="“16%”">L</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
        </tr>
        <tr>
            <td width="“18%”">Nigeria</td>
            <td style="text-align: center;" td="" width="“18%”">1993</td>
            <td style="text-align: center;" td="" width="“16%”">R</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
        </tr>
        <tr>
            <td width="“18%”">Indonesia</td>
            <td style="text-align: center;" td="" width="“18%”">1998</td>
            <td style="text-align: center;" td="" width="“16%”">L</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
        </tr>
        <tr>
            <td colspan="6"><strong><em>2000–2009 (incl. Color Revolutions)</em></strong></td>
        </tr>
        <tr>
            <td width="“18%”">Serbia</td>
            <td style="text-align: center;" td="" width="“18%”">2000</td>
            <td style="text-align: center;" td="" width="“16%”">L</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
        </tr>
        <tr>
            <td width="“18%”">Kyrgyzstan</td>
            <td style="text-align: center;" td="" width="“18%”">2005</td>
            <td style="text-align: center;" td="" width="“16%”">L</td>
            <td style="text-align: center;" td="" width="“16%”">Yes</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
        </tr>
        <tr>
            <td width="“18%”">Burma</td>
            <td style="text-align: center;" td="" width="“18%”">2007</td>
            <td style="text-align: center;" td="" width="“16%”">R</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
        </tr>
        <tr>
            <td width="“18%”">Iran</td>
            <td style="text-align: center;" td="" width="“18%”">2009</td>
            <td style="text-align: center;" td="" width="“16%”">K</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
            <td style="text-align: center;" td="" width="“16%”">No</td>
        </tr>
        <tr>
            <td colspan="6"><strong><em>2010–14 (incl. Arab Uprisings)</em></strong></td>
        </tr>
        <tr>
            <td width="“18%”">Bahrain</td>
            <td style="text-align: center" td="" width="“18%”">2011</td>
            <td style="text-align: center" td="" width="“16%”">R</td>
            <td style="text-align: center" td="" width="“16%”">No</td>
            <td style="text-align: center" td="" width="“16%”">No</td>
            <td style="text-align: center" td="" width="“16%”">No</td>
        </tr>
        <tr>
            <td width="“18%”">Egypt</td>
            <td style="text-align: center" td="" width="“18%”">2011</td>
            <td style="text-align: center" td="" width="“16%”">C</td>
            <td style="text-align: center" td="" width="“16%”">Yes</td>
            <td style="text-align: center" td="" width="“16%”">No</td>
            <td style="text-align: center" td="" width="“16%”">No</td>
        </tr>
        <tr>
            <td width="“18%”">Libya</td>
            <td style="text-align: center" td="" width="“18%”">2011</td>
            <td style="text-align: center" td="" width="“16%”">R</td>
            <td style="text-align: center" td="" width="“16%”">Yes</td>
            <td style="text-align: center" td="" width="“16%”">No</td>
            <td style="text-align: center" td="" width="“16%”">No</td>
        </tr>
        <tr>
            <td width="“18%”">Syria</td>
            <td style="text-align: center" td="" width="“18%”">2011</td>
            <td style="text-align: center" td="" width="“16%”">R</td>
            <td style="text-align: center" td="" width="“16%”">No</td>
            <td style="text-align: center" td="" width="“16%”">No</td>
            <td style="text-align: center" td="" width="“16%”">No</td>
        </tr>
        <tr>
            <td width="“18%”">Tunisia</td>
            <td style="text-align: center" td="" width="“18%”">2011</td>
            <td style="text-align: center" td="" width="“16%”">L</td>
            <td style="text-align: center" td="" width="“16%”">Yes</td>
            <td style="text-align: center" td="" width="“16%”">Yes</td>
            <td style="text-align: center" td="" width="“16%”">Yes</td>
        </tr>
        <tr>
            <td width="“18%”">Yemen</td>
            <td style="text-align: center" td="" width="“18%”">2011</td>
            <td style="text-align: center" td="" width="“16%”">R</td>
            <td style="text-align: center" td="" width="“16%”">Yes</td>
            <td style="text-align: center" td="" width="“16%”">No</td>
            <td style="text-align: center" td="" width="“16%”">No</td>
        </tr>
        <tr>
            <td width="“18%”">Egypt</td>
            <td style="text-align: center" td="" width="“18%”">2013</td>
            <td style="text-align: center" td="" width="“16%”">C</td>
            <td style="text-align: center" td="" width="“16%”">Yes</td>
            <td style="text-align: center" td="" width="“16%”">No</td>
            <td style="text-align: center" td="" width="“16%”">No</td>
        </tr>
        <tr>
            <td width="“18%”">Burkina Faso</td>
            <td style="text-align: center" td="" width="“18%”">2014</td>
            <td style="text-align: center" td="" width="“16%”">C</td>
            <td style="text-align: center" td="" width="“16%”">Yes</td>
            <td style="text-align: center" td="" width="“16%”">Yes</td>
            <td style="text-align: center" td="" width="“16%”">n/a</td>
        </tr>
    </tbody>
</table>


Table. Military Responses and Political Outcomes in Nonviolent Mass Protests, 1946–2014

Note: L = Loyalty Shift; R = Repression; C = Military Coup. A detailed description of how endgames were identified is reported in the Appendix. Leader exit based on Archigos Dataset v4.1. Transition to democracy and democracy status based on Polity2 value (Polity2 value of +6 or higher).

Cases that we count from 2011 through 2014 – these are the Arab Spring cases plus that of the small West African country of Burkina Faso in 2014 – the military chose repression or a coup in every instance save one. That sole exception came in Tunisia in 2011, when as we have seen the Tunisian army signaled its support for the protesters and induced the dictator to accept exile in Saudi Arabia.

The endgames occurred in every world region that has a significant number of autocracies, with clusters taking place in (post)communist Eurasia (the "velvet revolutions" and "color revolutions"), the Asia Pacific ("people power revolutions"), and the broader Middle East (the Arab Spring cases). These are also the regions where, as researchers have recently noted, civil-resistance campaigns have been most likely to be met with military repression.

Popular protests in authoritarian regimes can lead not only to different reactions from military establishments, but also to vastly different outcomes in terms of leader exit, regime survival, and democratization. As one might expect, a loyalty shift most often means that the authoritarian ruler will be gone within a year. The only cases of our forty in which this failed to happen came in Albania in 1990 and Madagascar in 1991. In the former, mass protests toward the end of the year forced communist ruler Ramiz Alia to initiate political liberalization, yet he did not resign the presidency until after the opposition's sweeping victory in the March 1992 parliamentary balloting. In Madagascar, President Didier Ratsiraka conceded to protesters' demands in October 1991 but stayed until he lost the February 1993 presidential runoff to opposition leader Albert Zafy.

Equally important, loyalty shifts strongly correlate with a transition to democracy within three years, as well as the presence, within five years after the endgame, of a democratic political regime. In the fourteen loyalty shifts found among the Table's forty cases, the only exceptions to these correlations are South Korea (1960), Albania (1990), and Kyrgyzstan (2005). In that last case, the Tulip Revolution pushed authoritarian president Askar Akayev out of office, but his successor ruled as a dictator until armed riots toppled him in 2010. In South Korea, the demise of the Syngman Rhee regime in the course of the 1960 student protests led to a brief democratic interregnum before General Park Chung-hee organized a military coup and institutionalized a form of personal rule that lasted until his assassination in 1979.

Military coups always cause leader exit, although "the perpetrators of coups tend to oust dictators only to impose new ones". Egypt's trajectory after the 2011 uprising is a typical example. The Supreme Council of the Armed Forces took power after pressing Mubarak to quit the presidency in February 2011. The military permitted elections, but in July 2013, General Abdel Fattah al-Sisi ousted President Mohamed Morsi in another coup. In 2014, Sisi secured election as president, and he was reelected in March 2018 while an ailing Morsi watched from a prison cell. In the Table's six coups, spanning the years from 1969 (Pakistan) to 2014 (Burkina Faso), the toppling of a dictator four times led to the installation of a fresh dictator. Even where a transition to democracy followed a military coup, as in Pakistan in 1969 and in Sudan in 1985, the democratic regime was short-lived and fell to another military intervention.

Yet repression alone cannot make an autocrat's tenure secure. In twelve of the nineteen cases where militaries used brutal force, the incumbent nevertheless fell, most often due to pressure from inside the regime coalition and shifting patterns of political allegiance and power. In Poland in 1970, for example, the army and the national police bloodily suppressed a shipyard-workers' uprising, but First Secretary Władysław Gomułka's career could not survive the troubles – his colleagues in the communist Politburo soon pushed him out. Similarly, the Iranian monarchy collapsed despite military repression when the ailing Shah Mohammad Reza Pahlavi fled into exile in 1978.

In the cases of Venezuela in 1958 and Thailand in 1973, repression did not prevent a transition to democracy. In the former, a violent crackdown on the popular uprising by parts of the deeply factionalized armed forces could not save the military dictatorship. Internal rivalries and widespread discontent with President Marcos Pérez Jiménez triggered mutinies and defections among the disgruntled soldiers. Having lost the loyalty of vast parts of the military, Pérez Jiménez fled into exile. In the latter, when students in Bangkok inspired large crowds to challenge Field Marshal Thanom Kittikachorn's rule, soldiers used lethal force against them. Yet King Bhumibol Adulyadej intervened, and Thanom soon resigned as premier and left the country. Almost two decades later, in May 1992, hundreds of thousands of Thai citizens demonstrated against the military junta led by General Suchinda Kraprayoon. The military cracked down hard, but again the palace weighed in and made the dictator step down.

Overall, fewer than half of all endgames (48 percent) led to a democratic transition, while in only 45 percent of all endgame cases was a democracy in place five years after this scenario's key events. In the 1980s and early 1990s, scholars of transition expressed skepticism about the value of pressure from below and even portrayed mass mobilization as likely to harm democratization prospects. Yet more recent students of "democratic revolutions" and nonviolent campaigns argue that popular pressure has in case after case been a key to bringing about political liberalization and subsequent democratization. Another and still more recent study has suggested that military coups or even the threat of same can help to usher in democracy, though statistical analysts have disputed this notion.

Our own analysis, which deals only with nonviolent mass protests, cannot resolve that controversy. Yet our work does offer three major insights. First, we have shown that when military leaders opt for a loyalty shift in response to nonviolent mass protests, the implications for subsequent democratization are positive. Loyalty shifts have a good record of producing transitions to democracy within three years, and of sustaining democratization. This is true even though the involvement of military elites in regime change can spawn new problems for democratization. These may include the creation of "reserved domains" that exempt the military from civilian authority, not to mention the acquisition by generals of veto power over key political decisions.

In seven of the fourteen countries in which a loyalty shift opened the door to a democratic transition, military establishments were able to carve out significant privileges as that transition took place. Military coups overthrew democratic governments in South Korea in 1961 and in Madagascar in 2009, while in Argentina and the Philippines democratic governments struggled to survive a series of military coup attempts and mutinies. In Bangladesh, Indonesia, and post-1987 South Korea as well, democratic transitions failed (at least in the short term) to bring the armed forces fully under the control of democratically chosen civil authorities. Only in Malawi and some communist or postcommunist countries, such as Czechoslovakia, East Germany, Kyrgyzstan, Romania, and Serbia, did military leaders fail to claim a special niche during and after the transition to democracy. These were all societies in which the military historically wielded little political power.

Our second insight is that the empirical evidence provided by dictators' endgames does not support the idea that coups improve prospects for democracy. Unlike a loyalty shift, a military coup during a nonviolent revolution has only rarely opened a path to sustained democratization. Rather, the typical military coup during an endgame leads to some form of renewed authoritarianism.

Finally, our finding that nearly four-fifths (78 percent) of all endgames lead to the dictator's exit points to something important: Dictators who fail to forestall popular mobilizations are ripe for takedown. The very fact of sustained mass protests signals a dictator's weakness. It becomes highly likely either that the troops will refuse to act against the demonstrators or that the generals will step in and force a change of ruler. Even if a dictator can motivate the military to attack the protesters, there is still a high chance that the dictator will wind up losing office and suffering exile or perhaps even death.

---

## Explaining Military reactions

What, then, can tell us how militaries will respond to popular protests? Recent studies have suggested some answers. The factors to be weighed include the type of authoritarian regime; the strategies carried out by autocrats to prevent military groups from seizing power via a coup d'état ("coup-proofing"); the diversity of the social movement and the social distance between soldiers and protesters; and whether soldiers were responsible for large-scale human-rights violations prior to the endgame.

### Type of authoritarian regime

The data in the Table in the Appendix (online) suggest that endgames in military regimes hold a higher likelihood of military repression compared to endgames in other types of autocratic regimes. In nine of twelve endgames that occurred in military-led regimes, the military-as-institution used violence to defend the military-as-government - for instance during the "Black May" (1992) crackdown by the Thai military, which killed dozens of protesters, injured hundreds more, and placed thousands in detention. By contrast, only in five of fourteen endgames in party-led dictatorships (36 percent) and three of twelve endgames in personalistic regimes did the military respond with repression. These findings are consistent with the broader scholarly literature on state repression, which emphasizes that regimes dominated by specialists in managing violence are less likely to be good at stopping mass protest before it starts, and are more likely to react with sweeping violence if and when large-scale protest does occur.

### Coup-proofing

Dictators can apply a wide variety of coup-proofing measures. The most common are counterbalancing ("structural coup-proofing") and ascriptive selection (that is, stacking key units with ethnic or other loyalists).

Counterbalancing involves the creation of parallel military organizations and aims to hamper the military's potential to act in a unified and coherent way against the dictator. Of the 40 dictatorships surveyed, 28 had at least one security agency controlling, spying on, or otherwise balancing the military. Examples include the Revolutionary Guards in Iran and the Seguridad Nacional of military-ruled Venezuela (1952–58).

Ascriptive selection aims to maximize military loyalty by staffing a part or the whole of the military with soldiers from the dictator's own ethnic group, religious sect, tribe, or region. This mechanism is especially, but not exclusively, effective in ethnic-minority regimes. An example of this model is the small island kingdom of Bahrain. There, the Sunni royal family has recruited a mercenary army filled with Sunnis from outside Bahrain to keep a mostly Shia society in line. In Syria, the Assad family filled senior and sensitive military posts with relatives, members of the same minority religious sect (the Alawites), members of other religious minorities, and natives of the Assads' home region.

In South Korea, one of the world's most ethnically homogenous societies, dictators Park Chung-hee and Chun Doo-hwan (Park's successor) favored a certain group of graduates from the Korea Military Academy. These were the members of the hanahoe or "Number One" faction, a group of officers who hailed almost exclusively from the two presidents' shared home province of Kyongsang in the southeast of the country. By contrast, neither Tunisia's Ben Ali nor Egypt's Mubarak (each of whom had a military background) infiltrated the armed forces with personal loyalists from a particular ethnic, religious, or regional group.

Counterbalancing carries the risk of harming military effectiveness by sowing rivalries within the armed forces, and it can breed resentment among parts of the military that feel disfavored. Loyalist-stacking is a surer means of keeping the military "on side," for it cuts off military leaders from the junior soldiery and the broader society and ties their survival prospects to those of the dictator. In fact, the data in Table 2 suggest that counterbalancing is a blunt instrument for dictators trying to survive their endgame: Only 12 of the 28 dictators in our dataset who used some form of counterbalancing actually induced their militaries to defend them when the crisis came. Rather, counterbalancing might actually breed military grievances and thereby make coup-plotting more likely. Something like this appears to have happened in Egypt (2011) and four other cases. In eleven additional cases where dictators had been using counterbalancing, including those of Romania (1989) and Tunisia (2011), militaries shifted their loyalty.

Compare this to the record of ascriptive selection. In eleven of the seventeen cases where military leaders had been specially selected to ensure their loyalty, the armed forces cracked down on the protests. Except for a few cases such as the Philippines (1986), South Korea (1987), and Kyrgyzstan (2005), militaries whose top ranks were stacked with loyalists stuck with the dictator throughout the crisis.

### The composition of nonviolent protest movements

Some scholars argue that how a military will react to popular protests depends on the internal characteristics of the opposition movement. According to this reasoning, demonstrators who are nonviolent, who rally in urban settings, and who come from an ethnoreligious, regional, or class background which resembles that of many soldiers will be less likely to face harsh military action. Indeed, the existence of affinities and similarities between the troops and the protesters will raise the probability that military officers and opposition leaders will begin to form common networks. It will also help if the demonstrations are broad-based and demographically diverse, featuring women as well as men and involving citizens of varying ages.

Orders to unleash violence against a movement that meets such criteria could fill military leaders with worry that discipline will collapse if repression is tried. Given how much value militaries place upon cohesion, such a movement will have a good chance of inducing the military to shift its loyalty rather than resort to arms in the streets. Looking at things from the dictator's point of view, conversely, one can see the reason for loyalist-stacking. Staffing senior ranks and elite units (or perhaps even the whole military) with members of a regime-aligned ethnic or other minority will widen the social distance between the military and the general populace and preclude networking. Demonstrators can be disparaged as mischiefmakers, radicals, and "enemies of the nation".

The protests that broke out in South Korea in May 1980 featured leftist students, members of illegal unions, and locals in the city of Kwangju, the capital of Cholla Province in the southwestern corner of the peninsula. President Chun Doo-hwan sent troops from Kyongsang to put them down; the bloody clashes that ensued cost hundreds of lives. In Bahrain, the Arab Spring protesters were mainly Shia while those who suppressed them were Sunni. In Syria, a "minoritized" senior officer corps and set of crack units attacked demonstrators who came mainly from the country's disadvantaged and politically marginalized Sunni majority.

Seven years after the failed Kwangju demonstrations, South Korea's successful democratization movement manifested itself in all regions of the country and drew support from a broad cross-section of society, including the urban middle classes. Much the same was true of the 1986 "people power" movement in the Philippines and the so-called 8-8-88 Uprising in Burma. In the former, most of the action unfolded in Manila, but the effort to oust dictator Ferdinand Marcos united the poor and the middle classes, workers, social activists, business groups, the Catholic Church, and the communist-led National Democratic Front.

While social diversity might explain different patterns of military responses vis-à-vis unarmed (as distinguished from armed and hence less diverse) movements, a nonviolent movement's internal diversity does not seem to be a good predictor of how a military establishment will respond to it. In 19 of 21 cases where there was a loyalty shift, the nonviolent campaign was diverse, inclusive, and not too different from the military in its composition. Yet much the same was true in 15 of the 19 cases where repression occurred. Movement diversity tells us little about how a military is likely to react to a protest movement.

## Past human-rights violations by the military

A third factor is whether military leaders must worry that the dictator's downfall will lead to the exposure and punishment of human-rights violations committed under the authoritarian regime. Military leaders who can be confident that they will not face such a risk are more likely to go over to the protesters' side. Hence a military with little or no record of involvement in torture, extralegal killings, or forced population movements is more likely to defect. The armies of the East European communist countries and Ben Ali's Tunisia, for example, mostly stayed out of internal-security work - the regimes had other, specialized agencies to handle it - and could not be counted among dictatorship's mainstays. By contrast, the militaries of Burma and Syria will have much to fear if accountability ever comes to either of those places.

There have been exceptions, of course. The armed forces of Argentina had been waging a covert "dirty war" against their own society for years, yet chose to retreat from power rather than repress after losing the Falklands War to Great Britain in 1982. In communist Poland (1956) and China (1989), conversely, the armies had no history of large-scale human-rights abuses yet gunned down demonstrators when called in to do so.

Overall, however, the data support the idea that having a military with "clean hands" makes repression less likely: Of the 25 militaries in our dataset with troubling human-rights records, 14 tried to repress protests. Those represent, moreover, three-fourths of all our repression cases. Looking at the six coups in our set of instances, we see that tarnished militaries were behind five of them.

Endgames can result in a wide variety of outcomes. This is true in terms of both military responses and the short- and medium-term effects that we see in the area of leader survival versus regime change. No single factor can perfectly predict how a military will respond to a dictator's endgame, and some factors seem hardly to have any explanatory power at all. At the very least, this suggests that military responses to dictators' endgames are likely to be shaped by causal factors that interact in complex, nondeterministic ways. In addition to the factors discussed above, scholars have proposed other possible explanations that revolve around the military's formal or informal missions and its participation in international alliances and officer-exchange programs with democratic countries and their armies. Conceptualizing many of these factors is a challenge, however, to say nothing of the task of collecting reliable empirical data of the sort needed to make enlightening comparisons.

Dictators' endgames are complicated and chaotic affairs. Leaders on all sides must contend with limited information and high uncertainty. The risk of mistakes, miscalculations, and ad hoc decisions is high, and no single theoretical model is likely to be able to capture and explain it all. Nonetheless, we believe that embracing the challenge of more rigorous theoretical analysis can advance our understanding of the dynamics of dictators' endgames as well as their relevance for the political development and (possible) democratic future of authoritarian regimes.

Mindful of the risks of overgeneralizing from a limited number of cases, we nonetheless believe that if one knows enough about how each of the factors discussed here manifests itself in a given country, one can make a highly educated guess about how that country's military will respond to an endgame scenario. We cannot predict if and when such a scenario will occur, but we can assist international actors who want to support democratization in grasping how they can best promote democracy-friendly conditions once a nonviolent revolution breaks out. Knowing who the different types of actors are and what their incentives and interests are likely to be could prove valuable indeed as Western decision makers search for measures that will give the democratic possibility its best chance of success in an uncertain world.


