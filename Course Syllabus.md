# Course Syllabus

## Welcome to HIST362: Modern Revolutions

Specific information about this course and its requirements can be found below. For more general information about taking Saylor Academy courses, including information about Community and Academic Codes of Conduct, please read the Student Handbook.

## Course Description

Explore political and social revolutions and modern independence movements by studying the causes of these social upheavals and how they have shaped our modern world. 

## Course Introduction

How do we define revolution? In 1970, the Chinese leader Zhou Enlai said that the outcome of the French Revolution was unclear – 200 years is too soon to determine its long-term effects! Revolutions are complex, nuanced, and can shift the global balance in fundamental ways – from England's Glorious Revolution in the 1600s to the United States, France, Haiti, Russia, China, and the modern independence movements in Southeast Asia, Africa, Latin America, and the Middle East. In this course, we explore the causes of revolution, analyze the ideologies that inspired the revolutionaries, examine the use of violence, and consider how historical revolutions have shaped contemporary politics. Our exploration includes reading and evaluating critical historical sources.

Most revolutionaries rose to protest a failure of Thomas Hobbes' Social Contract – these instigators felt their government no longer served the needs of its people or had become unresponsive and oppressive. The protesters use different methods – from relatively peaceful civil disobedience in India; to extreme violence in France, Russia, China, and Cambodia; to subversive terrorist tactics against colonialism in the United States, Latin America, Vietnam, and the modern Middle East.

Although the jury is still out, several revolutions created stable representative governments, such as in India, Japan, the United Kingdom, and the United States. Did China and Russia create stable governments, or did the autocratic leaders simply stifle discontent? As Zhou Enlai said, history will decide. Many countries elected or appointed a series of failed leaders who prompted counter-revolutionaries to rise in civil war and subsequent revolution. This occurred in France, Haiti, Latin America, and the Middle East. Many of the revolutions we study in this course were direct responses to colonialism and European imperialism, such as in the United States, Haiti, Latin America, Southeast Asia, Vietnam, India, and the Middle East.

Revolutions come in many forms: political, social, agricultural, and scientific. We begin by examining the nature of political revolution and how pre-revolutionary Europe and the Enlightenment have shaped modern revolutions. We will also explore European colonialism and imperialism since the oppression the foreign powers caused prompted revolutionaries to rebel in search of independence. By the end of this course, you will be able to discuss the nature of political revolution, identify commonalities and differences among these events, and understand how they individually and collectively transformed the modern world.

This course includes the following units:

- Unit 1: The Glorious Nature of Revolution
- Unit 2: The American Revolution
- Unit 3: The French Revolution and Its Legacy
- Unit 4: Revolution in Haiti, Mexico, Latin America, and the Philippines 
- Unit 5: Nations, Empires, Imperialism, and Peoples in an Industrializing Age
- Unit 6: The Russian Empire and Revolutions of 1905 and 1917 and Their Legacy 
- Unit 7: The Effects of Colonialism on Asia
- Unit 8: The Collapse of Empires in the Middle East and Asia
 

## Course Learning Outcomes

Upon successful completion of this course, you will be able to:

- provide a concise historical narrative of each of the revolutions presented in the course;
- compare the origins and causes of each revolution;
- compare the goals and ideals of the revolutionaries in various modern revolutions;
- discuss how various world revolutions have affected the rights of women and members of the working class;
- discuss the patterns and dynamics of revolutionary violence and how revolutionaries used nonviolent tactics against oppressive regimes;
- describe connections between revolutionary ideologies and revolutionary events;
- analyze the impact of each revolution on modern politics;
- describe competing theoretical models of revolutionary change; and
- review the ideas presented in primary historical documents.

Throughout this course, you will also see learning outcomes in each unit. You can use those learning outcomes to help organize your studies and gauge your progress.
 

## Course Materials

The primary learning materials for this course are articles, lectures, and videos.

All course materials are free to access and can be found in each unit of the course. Pay close attention to the notes that accompany these course materials, as they will tell you what to focus on in each resource, and will help you to understand how the learning materials fit into the course as a whole. You can also see a list of all the learning materials in this course by clicking on Resources in the navigation bar.

## Evaluation and Minimum Passing Score

Only the final exam is considered when awarding you a grade for this course. In order to pass this course, you will need to earn a 70% or higher on the final exam. Your score on the exam will be calculated as soon as you complete it. If you do not pass the exam on your first try, you may take it again as many times as you want, with a 7-day waiting period between each attempt. Once you have successfully passed the final exam you will be awarded a free Course Completion Certificate.

There are also end-of-unit assessments in this course. These are designed to help you study, and do not factor into your final course grade. You can take these as many times as you want until you understand the concepts and material covered. You can see all of these assessments by clicking on Quizzes in the course's navigation bar.

## Tips for Success

HIST362: Modern Revolutions is a self-paced course, which means that you can decide when you will start and when you will complete the course. There is no instructor or an assigned schedule to follow. We estimate that the "average" student will take 86 hours to complete this course. We recommend that you work through the course at a pace that is comfortable for you and allows you to make regular progress. It's a good idea to also schedule your study time in advance and try as best as you can to stick to that schedule.

Learning new material can be challenging, so we've compiled a few study strategies to help you succeed:

Take notes on the various terms, practices, and theories that you come across. This can help you put each concept into context, and will create a refresher that you can use as you study later on.
As you work through the materials, take some time to test yourself on what you remember and how well you understand the concepts. Reflecting on what you've learned is important for your long-term memory, and will make you more likely to retain information over time.
 

## Suggested Prerequisites

In order to take this course, you should:

have completed HIST103: [World History in the Early Modern and Modern Eras (1600–Present)](https://learn.saylor.org/course/view.php?id=31)
 

## Technical Requirements

This course is delivered entirely online. You will be required to have access to a computer or web-capable mobile device and have consistent access to the internet to either view or download the necessary course resources and to attempt any auto-graded course assessments and the final exam.

To access the full course including assessments and the final exam, you will need to be logged into your Saylor Academy account and enrolled in the course. If you do not already have an account, you may create one for free here. Although you can access some of the course without logging in to your account, you should log in to maximize your course experience. For example, you cannot take assessments or track your progress unless you are logged in.
For additional guidance, check out Saylor Academy's FAQ.


## Fees

This course is entirely free to enroll in and to access. Everything linked in the course, including textbooks, videos, webpages, and activities, is available for no charge. This course also contains a free final exam and course completion certificate.